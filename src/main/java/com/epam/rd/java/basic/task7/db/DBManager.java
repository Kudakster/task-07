package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class DBManager {

    private static DBManager instance;

    private static Connection connection;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        String URL = null;

        try {
            InputStream inputStream = new FileInputStream("app.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            URL = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> userList = new LinkedList<>();
        String SQL = "SELECT * FROM users";

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(SQL);
            while (resultSet.next()) {
                User user = User.createUser((String) resultSet.getObject("login"));
                user.setId((int) resultSet.getObject("id"));
                userList.add(user);
            }
            resultSet.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return userList;
    }

    public boolean insertUser(User user) throws DBException {
        String SQL = "INSERT INTO users VALUES (DEFAULT, '" + user.getLogin() + "')";

        try {
            connection.createStatement().execute(SQL);
        } catch (SQLException throwable) {
            throw new DBException("", throwable);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {

        Arrays.stream(users).map(User::getLogin).forEach(login -> {
            String SQL = "DELETE FROM users WHERE login = '" + login + "'";

            try {
                connection.createStatement().execute(SQL);
            } catch (SQLException throwable) {
            }
        });
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        String SQL = "SELECT * FROM users WHERE login = '" + login + "'";

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(SQL);
            if (resultSet.next()) {
                user.setLogin((String) resultSet.getObject("login"));
                user.setId((int) resultSet.getObject("id"));
                resultSet.close();
                return user;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        String SQL = "SELECT * FROM teams WHERE name = '" + name + "'";

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(SQL);
            if (resultSet.next()) {
                team.setName((String) resultSet.getObject("name"));
                team.setId((int) resultSet.getObject("id"));
                resultSet.close();
                return team;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teamList = new LinkedList<>();
        String SQL = "SELECT * FROM teams";

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(SQL);
            while (resultSet.next()) {
                Team team = Team.createTeam((String) resultSet.getObject("name"));
                team.setId((int) resultSet.getObject("id"));
                teamList.add(team);
            }
            resultSet.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return teamList;
    }

    public boolean insertTeam(Team team) throws DBException {
        String SQL = "INSERT INTO teams VALUES (DEFAULT, '" + team.getName() + "')";
        try {
            connection.createStatement().execute(SQL);
        } catch (SQLException throwable) {
            throw new DBException("", throwable);
        }
        SQL = "SELECT * FROM teams WHERE name LIKE '" + team.getName() + "'";
        try {
            ResultSet resultSet = connection.createStatement().executeQuery(SQL);
            while (resultSet.next()) {
                team.setId((int) resultSet.getObject("id"));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Arrays.stream(teams).forEach(team -> {
            try {
                int userId = getUser(user.getLogin()).getId();
                int teamId = getTeam(team.getName()).getId();
                String SQL = "INSERT INTO users_teams VALUES (" + userId + ", " + teamId + ")";
                connection.createStatement().executeUpdate(SQL);
            } catch (SQLException | DBException e) {
            }
        });
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new LinkedList<>();

        try {
            String SQL = "SELECT * FROM users_teams WHERE user_id = " + getUser(user.getLogin()).getId();
            ResultSet resultSet = connection.createStatement().executeQuery(SQL);
            while (resultSet.next()) {
                ResultSet res = connection.createStatement().executeQuery("SELECT * FROM teams WHERE id = " + resultSet.getObject("team_id"));
                while (res.next()) {
                    teamList.add(Team.createTeam((String) res.getObject("name")));
                }
            }
            resultSet.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String SQL = "DELETE FROM teams WHERE name = '" + team.getName() + "'";
        try {
            connection.createStatement().execute(SQL);
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        String SQL = "UPDATE teams SET name = '" + team.getName() + "' WHERE id = " + team.getId();
        try {
            connection.createStatement().execute(SQL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

}
